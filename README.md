## Rails_build_dev_env
### Gitインストール
```
# yum -y install git
```

### Githubアカウント作成
下記URLからアカウント作成
https://github.com/

### rbenvインストール
Rubyのバージョン管理
```
# git clone https://github.com/sstephenson/rbenv.git ~/.rbenv
# echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bash_profile
# echo 'eval "$(rbenv init -)"' >> ~/.bash_profile
# exec $SHELL -l
# rbenv --version
rbenv 1.1.0-7-g755c820
# git clone https://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build
# rbenv install --list
```

### Rubyインストール
```
# yum -y install gcc
# yum install -y openssl-devel readline-devel zlib-devel
# rbenv install -v 2.4.0
# rbenv rehash
# rbenv versions
  2.4.0
# rbenv global 2.4.0
# ruby -v
ruby 2.4.0p0 (2016-12-24 revision 57164) [x86_64-linux]
```

### Railsインストール
```
# gem install rails -v 5.0.0.1
# cd /var/opt
# yum -y install sqlite-devel
# gem install sqlite3 -v '1.3.13'
# rails _5.0.0.1_ new hello_app
# cd hello_app/
# yum -y install gcc-c++
# vi Gemfile
Note -----------------------------
# gem 'therubyracer', platforms: :ruby
↓
gem 'therubyracer', platforms: :ruby
----------------------------------
# bundle install
# bundle exec rails s
```